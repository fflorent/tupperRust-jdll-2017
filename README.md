# TupperRust JdLL 2017

Bienvenue aux TupperRust des JdLL de l'an 2017 !

Ce TupperRust se déroule comme des travaux pratiques durant lesquels vous allez réalisez vos premiers programmes en Rust, ou durant lesquels vous serez challengés !

Le TP s'articule en 4 étapes listées ci-après et d'une demi-heure chacune (25 minutes de réalisation, 5 minutes de présentation d'une solution). À chacune des étapes, on vous propose d'implémenter les tests (après ou avant l'implémentation selon vos préférences). Oui, on teste, on doute et alors ??? :)

## Étape 1 : Hello World

Pour voir le sujet, [se rendre ici](https://framagit.org/fflorent/tupperRust-jdll-2017/tree/tp1).

## Étape 2 : Emprunts fictifs et crustacés

Pour voir le sujet, [se rendre ici](https://framagit.org/fflorent/tupperRust-jdll-2017/tree/tp2).

## Étape 3 : Votre première application Rocket

Pour voir le sujet, [se rendre ici](https://framagit.org/fflorent/tupperRust-jdll-2017/tree/tp3).
